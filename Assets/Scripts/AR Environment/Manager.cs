using UnityEngine;
using System.Collections;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class Manager : MonoBehaviour {
    private ARPlaneManager planeManager;

    [SerializeField]
    [Tooltip("Instantiates this prefab on a plane at the raycast on plane location.")]
    GameObject placedPrefab;

    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start() {
        planeManager = GameManager.instance.ARSessionOrigin.GetComponent<ARPlaneManager>();
        StartCoroutine(place());
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update() {
      //  placeObject();
    }

    public IEnumerator place(){
        while(true){
            yield return new WaitForSeconds(0.2f);
            if(placeObject()){
                yield break;
            }
        }
    }

    public void togglePlaneDetect() {
        planeManager.enabled = !planeManager.enabled;
    }

    public bool placeObject() {
        int a = planeManager.trackables.count;
        if (a <= 0) return false;
        foreach (ARPlane plane in planeManager.trackables) {
            Instantiate(placedPrefab, plane.center, default);
            
            togglePlaneDetect();
            plane.gameObject.SetActive(false);
            return true;
        }
        return false;
    }

  
}