﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;


public class RaycastOnPlane : MonoBehaviour {

    [SerializeField]
    GameObject hitOnPlane;
    private ARHandProcessor handProcessor;
    private Laze lazeOnSpace = default;
    private RaycastHit hit;

    [SerializeField]
    [Tooltip("enemy to test, delete when finsih")]
    GameObject enemy;

    void Start() {
        handProcessor = GetComponent<ARHandProcessor>();
        lazeOnSpace = new Laze();
        lazeOnSpace.enable();
    }

    void Update() {
        if (!tryGetRayData(out Ray ray)) return;
        if (Physics.Raycast(ray, out hit, 2)) {
            hitOnPlane.SetActive(true);
            hitOnPlane.transform.position = hit.point;
            lazeOnSpace.moveTo(handProcessor.CurrentHand.GetLandmark(0), hit.point);
        } else {
            hitOnPlane.SetActive(false);;
        }
    }

    private bool tryGetRayData(out Ray ray) {
        if (handProcessor == null || handProcessor.CurrentHand == null) {
            ray = default;
            Debug.Log("Can't provide Raycast");
            return false;
        }

        Vector3 origin = handProcessor.CurrentHand.GetLandmark(0);
        Vector3 a = handProcessor.CurrentHand.GetLandmark(5);
        Vector3 b = handProcessor.CurrentHand.GetLandmark(9);
        Vector3 c = handProcessor.CurrentHand.GetLandmark(13);
        Vector3 d = (a + b + c) / 3;
        Vector3 direction = d - origin;

        ray = new Ray(origin, direction);
        return true;
    }

    public void SpawnEnemy(){
        Instantiate(enemy, Map.instance.place.position, default);
    }

    public Laze LazeOnSpace { get => lazeOnSpace; set => lazeOnSpace = value; }
}
