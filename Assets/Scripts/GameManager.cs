﻿using System.Collections;
using System.Collections.Generic;
using MediapipeHandTracking;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
public class GameManager : MonoBehaviour
{
    #region Singletion
    public static GameManager instance;

    private void Awake() {
        instance = this;
    }

    #endregion

    public GameObject ARSessionOrigin;
    public GameObject HandTracking;
    public GameObject HandDraw;
    public GameObject Gaze;
    public ARFrameProcessor ARFrameProcessor;
    public CharacterCombat characterCombat;
  
}
