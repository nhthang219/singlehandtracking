using UnityEngine;
public class DepthCalculator {
    public static float BASE_DEPTH = 1;
    //khoảng cách cổ tay đên khớp ngón giữ khi depth = 1 trong tọa đọ unity, ước lượng thôi
    public static float BASE_DISTANCE = 2.5f;

    // cả bàn tay
    private static int[,] finger_couples = { { 0, 1 }, { 1, 2 }, { 2, 3 }, { 3, 4 }, { 0, 5 }, { 5, 6 }, { 6, 7 }, { 7, 8 }, { 0, 9 }, { 9, 10 }, { 10, 11 }, { 11, 12 }, { 0, 13 }, { 13, 14 }, { 14, 15 }, { 15, 16 }, { 0, 17 }, { 17, 18 }, { 18, 19 }, { 19, 20 } };

    // độ dài bàn tay
    private static int[,] middle_finger = { { 0, 9 }, { 9, 10 }, { 10, 11 }, { 11, 12 } };

    // 
    public static float ParseFrom(float[] arr) {
        // tổng khoảng cách giữa các đốt ngón tay
        float handLenght = 0;
        for (int i = 0; i < 20; i++) {
            Vector3 a = new Vector3(arr[3 * finger_couples[i, 1]], 1 - arr[3 * finger_couples[i, 1] + 1], arr[finger_couples[i, 1] * 3 + 2] / 80.0f);
            Vector3 b = new Vector3(arr[3 * finger_couples[i, 0]], 1 - arr[3 * finger_couples[i, 0] + 1], arr[finger_couples[i, 0] * 3 + 2] / 80.0f);
            handLenght += Vector3.Distance(a, b);
        }
        return BASE_DEPTH * BASE_DISTANCE / handLenght;
    }
}

