﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Map : MonoBehaviour
{
    public static Map instance;
    [SerializeField]
    public Transform place;

    [SerializeField]
    public NavMeshSurface surface;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        bakeMap();
    }

    public void bakeMap(){
        surface.BuildNavMesh();
    }

  
}
