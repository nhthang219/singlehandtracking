﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CharacterAnimator : MonoBehaviour {
	
	public Animator animator;

	NavMeshAgent navmeshAgent;
	CharacterCombat combat;

	protected virtual void Start() {
		navmeshAgent = GetComponent<NavMeshAgent> ();
		combat = GetComponent<CharacterCombat> ();
		combat.OnAttack += OnAttack;
		combat.OnRun += OnRun;
		combat.OnSense += OnSense;
	}

	protected virtual void OnAttack() {
		animator.SetTrigger("Attack");
	}

	protected virtual void OnRun(){
		animator.SetTrigger("Run");
	}

	protected virtual void OnSense(){
		animator.SetTrigger("Sense");
	}
}
