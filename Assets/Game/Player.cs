using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
/* This class just makes it faster to get certain components on the player. */

 public class Player : MonoBehaviour {

	#region Singleton

	public static Player instance;
	private Camera cam;

	void Awake ()
	{
		instance = this;
	}

	#endregion

	void Start() {
		playerStats.OnHealthReachedZero += Die;
		cam = Camera.main;
		transform.position = cam.transform.position;
	}

	public CharacterCombat playerCombatManager;
	public PlayerStats playerStats;

	/// <summary>
	/// Update is called every frame, if the MonoBehaviour is enabled.
	/// </summary>
	void Update()
	{
		transform.position = cam.transform.position;
	}

	void Die() {
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}


}
